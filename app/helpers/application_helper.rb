# frozen_string_literal: true

module ApplicationHelper
  include Decidim::Comments::CommentsHelper
  include Decidim::LayoutHelper

  def extended_navigation_bar(items, max_items: 8)
    return unless items.any?

    extra_items = items.slice((max_items + 1)..-1) || []
    active_item = items.find { |item| item[:active] }
    render partial: 'decidim/shared/extended_navigation_bar', locals: {
      items: items,
      extra_items: extra_items,
      active_item: active_item,
      max_items: max_items
    }
  end
end
